﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility.Events;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private GameObject _playerModel;

    [SerializeField]
    private Animator _playerAnimator;

    [SerializeField]
    private float duration = 5f;

    [SerializeField]
    private float rotationSpeed = 5f;

    private GameObject[] _tileList;

    [SerializeField]
    private int nextTileIndex;
    private Vector3 currentPos;

    private void Start()
    {
        _tileList = _gameManager.QuizManager.TileList;
    }

    // Assign this to submit button
    public void MoveToNextTile()
    {
        StartCoroutine(MovePlayer(_tileList[nextTileIndex].transform.position, duration));
        StartCoroutine(RotatePlayer(_tileList[nextTileIndex].transform.position));
    }


    [SerializeField]
    private VoidEvent _quizGameEvent;

    [SerializeField]
    private VoidEvent _sudokuEvent;

    [SerializeField]
    private VoidEvent _endEvent;

    IEnumerator MovePlayer(Vector3 targetPos, float duration)
    {
        float time = 0;
        currentPos = this.gameObject.transform.position;
        while (time < duration)
        {
            _playerAnimator.SetBool("isWalking", true);
            
            this.gameObject.transform.position = Vector3.Lerp(currentPos, targetPos, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        Tile currentTile = _tileList[nextTileIndex].GetComponent<Tile>();
        this.gameObject.transform.position = targetPos;
        if (nextTileIndex < _tileList.Length)
        {
            if (currentTile.thisTileType == Tile.TileType.sudokuTile)
            {
                _sudokuEvent.Raise();
            }
            else if (currentTile.thisTileType == Tile.TileType.endTile)
            {
                _endEvent.Raise();
            }
            else
            {
                _quizGameEvent.Raise();
            }
            nextTileIndex++;
        }

        _playerAnimator.SetBool("isWalking", false);
        yield break;
    }

    IEnumerator RotatePlayer(Vector3 targetPos)
    {
        Vector3 direction = (targetPos - _playerModel.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);

        while (currentPos != targetPos)
        {
            _playerModel.transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
            yield return null;
        }

        yield break;
    }
}
