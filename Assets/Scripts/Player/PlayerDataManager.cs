﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{
    [SerializeField]
    private PlayerData_SO _playerData;

    // TODO Get data from main menu UI;
    public void UpdateCategory(string currentCategory)
    {
        _playerData.SetCategory(currentCategory);
    }
}
