﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PlayerData", menuName = "Player/PlayerData")]
public class PlayerData_SO : ScriptableObject
{
    
    [SerializeField]
    private string _teamName;

    public string TeamName => _teamName;

    [SerializeField]
    private StringVariable _category;

    public StringVariable Category => _category;

    [SerializeField]
    private IntVariable _totalScore;

    [SerializeField]
    private FloatVariable _totalTimeTaken;

    public void SetTeamName(string teamName)
    {
        _teamName = teamName;
        Debug.Log("Team name = " + _teamName);
    }

    public void SetCategory(string category)
    {
        _category.runtimeValue = category;
        Debug.Log("Category = " + _category.runtimeValue);
    }

    public void SetTotalScore(int totalScore)
    {
        _totalScore.runtimeValue = totalScore;
        Debug.Log("Total Score = " + _totalScore.runtimeValue);
    }

    public void SetTotalTimeTaken(float totalTimeTaken)
    {
        _totalTimeTaken.runtimeValue = totalTimeTaken;
        Debug.Log("Total Time Taken = " + _totalTimeTaken.runtimeValue);
    }
}
