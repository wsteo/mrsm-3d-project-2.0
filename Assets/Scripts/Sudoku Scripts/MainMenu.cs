﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public Sprite easy;
    public Sprite medium;
    public Sprite hard;
    public string[] difficultySudokuArray;
    public int difficultyIndex = 0;

    public void UpdateDifficultyIndex()
    {
        difficultyIndex++;
    }

    public void SetSudoku()
    {
        string difficulty = difficultySudokuArray[difficultyIndex].ToLower(); 
        switch (difficulty){
            case "easy":
                EasyGame();
                break;

            case "medium":
                MediumGame();
                break;

            case "hard":
                HardGame();
                break;

            case "veryhard":
                VeryHard();
                    break;

            case "expert":
                Expert();
                break;

            default:
                break;
        }
    }

    public void EasyGame()
    {
        Settings.Background = easy;
        Settings.Missing = Random.Range(5, 7);
        SceneManager.LoadSceneAsync("Sudoku - Gameplay", LoadSceneMode.Additive);
    }

    public void MediumGame()
    {
        Settings.Background = medium;
        Settings.Missing = Random.Range(8, 12);
        SceneManager.LoadSceneAsync("Sudoku - Gameplay", LoadSceneMode.Additive);
    }

    public void HardGame()
    {
        Settings.Background = hard;
        Settings.Missing = Random.Range(15, 20);
        SceneManager.LoadSceneAsync("Sudoku - Gameplay", LoadSceneMode.Additive);
    }

    public void VeryHard()
    {
        Settings.Background = hard;
        Settings.Missing = Random.Range(25, 30);
        SceneManager.LoadSceneAsync("Sudoku - Gameplay", LoadSceneMode.Additive);
    }

    public void Expert()
    {
        Settings.Background = hard;
        Settings.Missing = Random.Range(40, 50);
        SceneManager.LoadSceneAsync("Sudoku - Gameplay", LoadSceneMode.Additive);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
