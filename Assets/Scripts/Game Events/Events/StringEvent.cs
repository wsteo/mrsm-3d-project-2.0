using UnityEngine;

namespace GameUtility.Events
{
    [CreateAssetMenu(fileName = "New StringEvent", menuName = "GameEvents/String Event")]
    public class StringEvent : BaseGameEvent<string>{}
}