using UnityEngine.Events;

namespace GameUtility.Events
{
    [System.Serializable]
    public class UnityStringEvent : UnityEvent <string> {}
}