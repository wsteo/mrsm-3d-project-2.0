using UnityEngine.Events;

namespace GameUtility.Events
{
    [System.Serializable]
    public class UnityVoidEvent : UnityEvent <Void> {}
}