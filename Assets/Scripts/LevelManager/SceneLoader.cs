﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private enum _availableScenes
    {
        SceneLoader = 0,
        MainMenu = 1,
        MainLevel = 2,
        SudokuGameplay = 3
    }

    List<AsyncOperation> scenesToLoad = new List<AsyncOperation>();


    private void Start()
    {
        LoadMainMenu();
    }

    private void LoadScene(_availableScenes availableScenes)
    {
        if (!isSceneLoaded(availableScenes))
        {
            SceneManager.LoadSceneAsync((int)availableScenes, LoadSceneMode.Additive);
        }
    }

    private void UnloadScene(_availableScenes availableScenes)
    {
        if (isSceneLoaded(availableScenes))
        {
            SceneManager.UnloadSceneAsync((int)availableScenes);
        }
    }

    private bool isSceneLoaded(_availableScenes availableScenes)
    {
        int numSceneLoaded = SceneManager.sceneCount;
        Scene targetScene = SceneManager.GetSceneByBuildIndex((int)availableScenes);
        for (int i = 0; i < numSceneLoaded; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (!(scene.name == targetScene.name) && targetScene.isLoaded)
            {
                return true;
            }
        }
        return false;
    }

    public void LoadMainLevel(AsyncOperation asyncOperation)
    {
        if (!isSceneLoaded(_availableScenes.MainLevel))
        {
            scenesToLoad.Add(SceneManager.LoadSceneAsync((int)_availableScenes.MainLevel, LoadSceneMode.Additive));
        }

        // StartCoroutine(LoadingScreen());

        LoadScene(_availableScenes.MainLevel);
    }

    public void LoadMainGameplay()
    {
        LoadScene(_availableScenes.MainLevel);
        UnloadScene(_availableScenes.MainMenu);
    }

    public void UnloadMainGameplay()
    {
        UnloadScene(_availableScenes.MainLevel);
    }

    public void LoadMainMenu()
    {
        if (!isSceneLoaded(_availableScenes.MainMenu))
        {
            scenesToLoad.Add(SceneManager.LoadSceneAsync((int)_availableScenes.MainMenu, LoadSceneMode.Additive));
        }
    }

    public void LoadSudoku()
    {
        LoadScene(_availableScenes.SudokuGameplay);
    }

    public void UnloadSudoku()
    {
        UnloadScene(_availableScenes.SudokuGameplay);
    }

}
