using System;
using UnityEngine;
using GameUtility.Events;
using TMPro;

public class MainMenuBehaviour : MonoBehaviour
{
    [SerializeField]
    private TMP_Dropdown _teamNameDropdown;

    [SerializeField]
    private PlayerData_SO _playerData;
    
    public void GetTeamName()
    {
        int dropdownMenuIndex = _teamNameDropdown.value;
        string teamName = _teamNameDropdown.options[dropdownMenuIndex].text;
        _playerData.SetTeamName(teamName);
    }
}