using UnityEngine;

[CreateAssetMenu(fileName = "New QuestionList", menuName = "Quiz System/QuestionList", order = 0)]
public class QuestionList : ScriptableObject
{
    [SerializeField]
    private string _questionListCategory;

    public string QuestionListCategory => _questionListCategory;
    
    [SerializeField]
    private QuestionData[] _questionDataList;

    public QuestionData[] QuestionDataList => _questionDataList;
}