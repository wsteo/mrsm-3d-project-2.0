using System;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Question", menuName = "Quiz System/QuestionData", order = 0)]
public class QuestionData : ScriptableObject
{

    [Header("Question Image")]
    [SerializeField]
    private Sprite _questionImage;
    public Sprite QuestionImage => _questionImage;

    [SerializeField]
    private float _questionImageWidth;
    public float QuestionImageWidth => _questionImageWidth;

    [SerializeField]
    private float _questionImageHeight;
    public float QuestionImageHeight => _questionImageHeight;

    [SerializeField]
    private float _questionImageScale = 1f;
    public float QuestionImageScale => _questionImageScale;

    [SerializeField]
    private bool _isQuestionImageEnable = false;

    public bool IsQuestionImageEnable => _isQuestionImageEnable;

    [Header("Question Information")]
    [SerializeField]
    [TextArea(10, 30)]
    private string _description;
    public string Description => _description;

    [SerializeField]
    [TextArea(5, 10)]
    private string _answer;
    public string Answer => _answer;

    [SerializeField]
    private int _points = 5;
    public int Points => _points;
}