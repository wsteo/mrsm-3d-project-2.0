﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizManager : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private PlayerData_SO _playerData;

    [SerializeField]
    private List<QuestionList> _questionListPool;

    [SerializeField]
    private QuestionList _questionList;

    [SerializeField]
    private GameObject[] _tileList;

    public GameObject[] TileList => _tileList;


    UIManager uiManager;
    private string answer;
    private int points;
    private int questionIndex = 0;

    private void Start()
    {

        for (int _questionListIndex = 0; _questionListIndex < _questionListPool.Count; _questionListIndex++)
        {
            if (_playerData.Category.runtimeValue == _questionListPool[_questionListIndex].QuestionListCategory)
            {
                _questionList = _questionListPool[_questionListIndex];
            }         
        }
        uiManager = _gameManager.UIManager;
        questionIndex = 0;
        uiManager.QuizUI.SetActive(false);
    }

    public void SetQuestion()
    {
        if (_tileList[questionIndex].GetComponent<Tile>().thisTileType == Tile.TileType.endTile || 
            questionIndex < 0 || questionIndex >= _questionList.QuestionDataList.Length)
        {
            Debug.Log("Cannot set question.");
            return;
        }

        uiManager.QuizUI.SetActive(true);
        uiManager.AnswerInput.text = "";
        QuestionData questionData = _questionList.QuestionDataList[questionIndex];
        Vector2 imageDimension = new Vector2(questionData.QuestionImageWidth, questionData.QuestionImageHeight);
        Vector3 imageScale = new Vector3(questionData.QuestionImageScale, questionData.QuestionImageScale, questionData.QuestionImageScale);

        uiManager.QuestionImageCropped.SetActive(false);
        if (questionData.IsQuestionImageEnable == true)
        {
            uiManager.QuestionImageCropped.SetActive(true);
            uiManager.QuestionImage.sprite = questionData.QuestionImage;
            uiManager.QuestionImage.rectTransform.sizeDelta = imageDimension;
            uiManager.PopoutImage.sprite = questionData.QuestionImage;
            uiManager.PopoutImage.rectTransform.sizeDelta = imageDimension;
            uiManager.PopoutImage.rectTransform.localScale = imageScale;
        }

        uiManager.QuestionDecription.text = questionData.Description;
        answer = questionData.Answer;
        answer = answer.Replace(" ","");

        points = questionData.Points;

        // Debug.Log("SET QUESTION!");
    }

    public void ValidAnswer()
    {
        string playerAnswer = uiManager.AnswerInput.text;
        playerAnswer = playerAnswer.Replace(" ","");

        if (answer == playerAnswer)
        {
            Debug.Log("Correct answer!");
            uiManager.QuizUI.SetActive(false);
            UpdateNextQuestion();
            _gameManager.ScoreManager.UpdateTotalScore(points);

            return;
        }

        Debug.Log("Wrong answer!");
        uiManager.QuizUI.SetActive(false);
        UpdateNextQuestion();
    }

    public void UpdateNextQuestion()
    {
        questionIndex++;
    }

    public void UpdateScoreboard()
    {
        uiManager.PlayerFinalScore.text = _gameManager.ScoreManager.TotalScore.runtimeValue.ToString();
        uiManager.PlayerTimeTaken.text = _gameManager.TimerManager.DisplayTime(_gameManager.TimerManager.TimerDuration - _gameManager.TimerManager.Timer);
        uiManager.TeamName.text = _playerData.TeamName;
        Debug.Log(_playerData.TeamName);
    }
}
