﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public enum TileType
    {
        startTile,
        quizTile,
        sudokuTile,
        endTile
    }

    public TileType thisTileType;

    // [SerializeField]
    // private GameEvent _gameEvent;

    // private bool isFirstCollisionTrigger = true;

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (isFirstCollisionTrigger)
    //     {
    //         if (other.gameObject.CompareTag("Player"))
    //         {
    //             isFirstCollisionTrigger = false;
    //             _gameEvent.Raise();
    //             Debug.Log("Collision triggered!");
    //         }
    //     }
    // }
}
