using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private IntVariable _totalScore;
    public IntVariable TotalScore => _totalScore;

    private void Start()
    {
        _totalScore.runtimeValue = _totalScore.InitialValue;
    }

    private void Update()
    {
        _gameManager.UIManager.TotalScore.text = _totalScore.runtimeValue.ToString();
        // Debug.Log("Current score: " + _totalScore.runtimeValue);
    }

    public void UpdateTotalScore(int score)
    {
        _totalScore.runtimeValue += score;
    }
}
