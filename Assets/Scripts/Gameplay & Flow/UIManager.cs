﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("HUD UI")]
    [SerializeField]
    private GameObject _hudUI;
    public GameObject HUDUI => _hudUI;
    
    [SerializeField]
    private TMP_Text _totalScore;
    public TMP_Text TotalScore => _totalScore;

    [SerializeField]
    private TMP_Text _timer;
    public TMP_Text Timer => _timer;

    [Header("Quiz UI")]
    [SerializeField]
    private GameObject _quizUI;

    public GameObject QuizUI => _quizUI;
    
    [SerializeField]
    private Image _questionImage;

    public Image QuestionImage => _questionImage;

    [SerializeField]
    private GameObject _questionImageCropped;

    public GameObject QuestionImageCropped => _questionImageCropped;

    [SerializeField]
    private TMP_Text _questionDescription;
    public TMP_Text QuestionDecription => _questionDescription;

    [SerializeField]
    private TMP_InputField _answerInput;

    public TMP_InputField AnswerInput => _answerInput;

    [SerializeField]
    private Image _popoutImage;
    public Image PopoutImage => _popoutImage;

    [Header("Complete Level UI")]
    [SerializeField]
    private GameObject _completeLevelUI;
    public GameObject CompleteLevelUI => _completeLevelUI;

    [SerializeField]
    private TMP_Text _teamName;

    public TMP_Text TeamName => _teamName;

    [SerializeField]
    private TMP_Text _playerTimeTaken;
    public TMP_Text PlayerTimeTaken => _playerTimeTaken;

    [SerializeField]
    private TMP_Text _playerFinalScore;
    public TMP_Text PlayerFinalScore => _playerFinalScore;

}
