﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private QuizManager _quizManager;
    public QuizManager QuizManager => _quizManager;

    [SerializeField]
    private UIManager _UIManager;
    public UIManager UIManager => _UIManager;

    [SerializeField]
    private ScoreManager _scoreManager;
    public ScoreManager ScoreManager => _scoreManager;

    [SerializeField]
    private TimerManager _timerManager;
    public TimerManager TimerManager => _timerManager;

    [SerializeField]
    private SudokuManager _sudokuManager;
    public SudokuManager SudokuManager => _sudokuManager;
}