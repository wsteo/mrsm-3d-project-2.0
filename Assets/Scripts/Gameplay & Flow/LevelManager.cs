﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public void LoadSudokuMinigame()
    {
        SceneManager.LoadScene("Sudoku - Main Menu", LoadSceneMode.Additive);
    }

    public void UnloadSudokuMinigame()
    {
        SceneManager.UnloadSceneAsync("Sudoku - Main Menu");
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("Example");
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
