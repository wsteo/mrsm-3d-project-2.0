using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerManager : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private float _timerDuration = 0f;

    public float TimerDuration => _timerDuration;

    [SerializeField]
    private float _timer = 0f;

    public float Timer => _timer;

    private string _timerText;

    public string TimerText => _timerText;

    [SerializeField]
    private bool _activateTimer;

    private UIManager _uIManager;

    private void Start()
    {
        _timer = _timerDuration;
        _uIManager = _gameManager.UIManager;
        // _timer = 300f;
    }

    private void Update()
    {
        if (_activateTimer && _timer >= 0)
            _timer -= Time.unscaledDeltaTime;
        else
            this.GetComponent<TimerManager>().enabled = false;
        
        _uIManager.Timer.text = DisplayTime(_timer);
    }

    private void OnEnable()
    {
        _activateTimer = true;
    }

    private void OnDisable()
    {
        _activateTimer = false;
        _gameManager.UIManager.HUDUI.SetActive(false);
        _gameManager.UIManager.QuizUI.SetActive(false);
        _gameManager.QuizManager.UpdateScoreboard();
        _gameManager.UIManager.CompleteLevelUI.SetActive(true);
    }

    public string DisplayTime(float currentTimer)
    {
        float minutes = Mathf.FloorToInt(currentTimer / 60);
        float seconds = Mathf.FloorToInt(currentTimer % 60);

        _timerText = string.Format("{0:00}:{1:00}", minutes, seconds);
        return _timerText;
        // Debug.Log(_timerText);
    }
}