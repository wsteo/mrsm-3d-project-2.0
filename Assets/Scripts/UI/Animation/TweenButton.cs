﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenButton : MonoBehaviour
{
    [SerializeField]
    private float tweenTime;

    [SerializeField]
    private GameObject _gameObject;

    private void OnEnable()
    {
        Tween();
    }

    public void Tween()
    {
        LeanTween.cancel(gameObject);

        Vector3 originalScale = _gameObject.transform.localScale;

        _gameObject.transform.localScale = originalScale;

        LeanTween.scale(gameObject, Vector3.one * 2, tweenTime).setEasePunch();
    }
}
