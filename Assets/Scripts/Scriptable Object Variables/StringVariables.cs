using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New StringVariable", menuName = "Scriptable Object Variables/StringVariable", order = 0)]
public class StringVariable : BaseVariable<string> {}