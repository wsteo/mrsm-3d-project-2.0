using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New FloatVariable", menuName = "Scriptable Object Variables/FloatVariable", order = 0)]
public class FloatVariable : BaseVariable<float> {}