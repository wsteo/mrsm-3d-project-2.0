using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New IntVariable", menuName = "Scriptable Object Variables/IntVariable", order = 0)]
public class IntVariable : BaseVariable<int> {}