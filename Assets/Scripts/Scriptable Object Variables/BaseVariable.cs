﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BaseVariable<T> : ScriptableObject, ISerializationCallbackReceiver 
{
    [SerializeField]
    private T _initialValue;

    public T InitialValue => _initialValue;

    [NonSerialized]
    public T runtimeValue;

    public void OnAfterDeserialize()
    {
        runtimeValue = _initialValue;
    }

    public void OnBeforeSerialize() {}
}
