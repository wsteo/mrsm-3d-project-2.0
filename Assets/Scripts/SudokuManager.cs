﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility.Events;

public class SudokuManager : MonoBehaviour
{
    private enum SudokuDifficulty
    {
        Easy, Medium, Hard, VeryHard, Expert
    }

    [SerializeField]
    private int _difficultyIndex = 0;

    [SerializeField]
    private SudokuDifficulty[] _difficultySudokuArray;

    [SerializeField]
    private IntVariable _missingSudokuTile;
    
    public void UpdateDifficultyIndex()
    {
        _difficultyIndex++;
    }

    public void SetSudoku()
    {
        SudokuDifficulty difficulty = _difficultySudokuArray[_difficultyIndex];
        switch (difficulty)
        {
            case SudokuDifficulty.Easy:
                EasyGame();
                break;

            case SudokuDifficulty.Medium:
                MediumGame();
                break;

            case SudokuDifficulty.Hard:
                HardGame();
                break;

            case SudokuDifficulty.VeryHard:
                VeryHard();
                break;

            case SudokuDifficulty.Expert:
                Expert();
                break;

            default:
                break;
        }
    }

    public void SetSudokuDifficulty(int minRange, int maxRange)
    {
        _missingSudokuTile.runtimeValue = Random.Range(minRange, maxRange);
        UpdateDifficultyIndex();
        Debug.Log("Sudoku Missing: " + _missingSudokuTile.runtimeValue);
    }

    public void EasyGame()
    {
        SetSudokuDifficulty(5, 7);
    }

    public void MediumGame()
    {
        SetSudokuDifficulty(8, 12);
    }

    public void HardGame()
    {
        SetSudokuDifficulty(15, 20);
    }

    public void VeryHard()
    {
        SetSudokuDifficulty(25, 30);
    }

    public void Expert()
    {
        SetSudokuDifficulty(40, 50);
    }
}
